package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Employee;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private List<Employee> employees = new ArrayList<>();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void parse() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler() {
				@Override
				public void startElement(String uri, String localName, String qName, Attributes attributes) {
					if (qName.equals("employee")) {
						employees.add(new Employee(attributes.getValue("ts:name"),
								attributes.getValue("ts:job")));
					}
				}
			};
			parser.parse(xmlFileName, handler);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	public void sort() {
		employees.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
	}

	public void write(String newXmlFileName) {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		try {
			XMLStreamWriter writer = factory.createXMLStreamWriter(new FileOutputStream(newXmlFileName));
			writer.writeStartDocument();
			writer.writeStartElement("ts:employees");
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xsi:schemaLocation", "http://com.my:8080 input.xsd");
			writer.writeAttribute("xmlns:ts", "http://com.my:8080");
			for (Employee employee : employees) {
				writer.writeEmptyElement("employee");
				writer.writeAttribute("ts:name", employee.getName());
				writer.writeAttribute("ts:job", employee.getJob());
			}
			writer.writeEndElement();
			writer.writeEndDocument();
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}