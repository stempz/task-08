package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Employee;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private List<Employee> employees = new ArrayList<>();

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void parse() {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		try {
			XMLEventReader reader = factory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					if (startElement.getName().getLocalPart().equals("employee")) {
						Attribute name = startElement.getAttributeByName(new QName("http://com.my:8080",
								"name"));
						Attribute job = startElement.getAttributeByName(new QName("http://com.my:8080",
								"job"));
						if (name != null && job != null)
							employees.add(new Employee(name.getValue(), job.getValue()));
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void sort() {
		employees.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
	}

	public void write(String newXmlFileName) {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		try {
			XMLStreamWriter writer = factory.createXMLStreamWriter(new FileOutputStream(newXmlFileName));
			writer.writeStartDocument();
			writer.writeStartElement("ts:employees");
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xsi:schemaLocation", "http://com.my:8080 input.xsd");
			writer.writeAttribute("xmlns:ts", "http://com.my:8080");
			for (Employee employee : employees) {
				writer.writeEmptyElement("employee");
				writer.writeAttribute("ts:name", employee.getName());
				writer.writeAttribute("ts:job", employee.getJob());
			}
			writer.writeEndElement();
			writer.writeEndDocument();
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}