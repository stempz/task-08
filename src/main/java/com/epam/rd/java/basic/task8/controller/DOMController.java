package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entities.Employee;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private List<Employee> employees = new ArrayList<>();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void parse() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document d = db.parse(xmlFileName);

			NodeList nl = d.getElementsByTagName("employee");
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				NamedNodeMap nnm = n.getAttributes();
				employees.add(new Employee(nnm.getNamedItem("ts:name").getNodeValue(), nnm.getNamedItem("ts:job").getNodeValue()));
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	public void sort() {
		employees.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
	}

	public void write(String newXmlFileName) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document d = db.newDocument();
			Element root = d.createElement("ts:employees");
			root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			root.setAttribute("xsi:schemaLocation", "http://com.my:8080 input.xsd");
			root.setAttribute("xmlns:ts", "http://com.my:8080");
			for (Employee employee : employees) {
				Element child = d.createElement("employee");
				child.setAttribute("ts:name", employee.getName());
				child.setAttribute("ts:job", employee.getJob());
				root.appendChild(child);
			}
			d.appendChild(root);

			Transformer trf = TransformerFactory.newInstance().newTransformer();
			DOMSource src = new DOMSource(d);
			StreamResult result = new StreamResult(new FileOutputStream(newXmlFileName));
			trf.transform(src, result);
		} catch (ParserConfigurationException | FileNotFoundException | TransformerException e) {
			e.printStackTrace();
		}
	}

}
